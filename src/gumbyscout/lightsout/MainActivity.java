package gumbyscout.lightsout;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements OnClickListener {
	// the button widgets
	private FlipButton[][] flipButtons;

	// dialog boxes
	AlertDialog winningBox;
	AlertDialog resetBox;

	// table in layout
	TableLayout tl;

	// board dimensions
	private final int WIDTH = 3;
	private final int HEIGHT = 3;

	// counter to store number of tiles flipped
	private int numCheckedTiles = 0;

	// counter to count number of moves
	private static int numMoves = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initializeWidgets();
	}

	public void initializeWidgets() {
		// assign the table layout
		tl = (TableLayout) findViewById(R.id.layout_table);

		// initialize the array
		flipButtons = FlipButton.generateFlipButtons(HEIGHT, WIDTH);

		/**
		 * broken dynamic board size, doesn't quite work the way I want it to
		 * visually for (int i = 0; i < HEIGHT; i++){ TableRow tr = new
		 * TableRow(this); //set layout stuff tr.setLayoutParams(new
		 * TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT,
		 * TableLayout.LayoutParams.MATCH_PARENT, 1.0f));
		 * 
		 * for (int k = 0; k < WIDTH; k ++){ flipButtons[i][k].setTb(new
		 * ToggleButton(this), this);
		 * 
		 * 
		 * 
		 * //set background selector and get rid of text
		 * flipButtons[i][k].tb.setBackgroundResource
		 * (R.drawable.selector_button); flipButtons[i][k].tb.setText("");
		 * flipButtons[i][k].tb.setTextOn("");
		 * flipButtons[i][k].tb.setTextOff("");
		 * 
		 * //set buttons to certain shape and look
		 * flipButtons[i][k].tb.setLayoutParams(new TableRow.LayoutParams(
		 * TableRow.LayoutParams.MATCH_PARENT,
		 * TableRow.LayoutParams.MATCH_PARENT, 1.0f));
		 * 
		 * //add to the new table row tr.addView(flipButtons[i][k].tb); }
		 * tl.addView(tr); }
		 **/

		// assign indexes to actual widgets in layout, hard coded, not how I
		// wanted to do it
		flipButtons[0][0].setTb(
				(ToggleButton) findViewById(R.id.toggleButton1), this);
		flipButtons[0][1].setTb(
				(ToggleButton) findViewById(R.id.toggleButton2), this);
		flipButtons[0][2].setTb(
				(ToggleButton) findViewById(R.id.toggleButton3), this);

		flipButtons[1][0].setTb(
				(ToggleButton) findViewById(R.id.toggleButton4), this);
		flipButtons[1][1].setTb(
				(ToggleButton) findViewById(R.id.toggleButton5), this);
		flipButtons[1][2].setTb(
				(ToggleButton) findViewById(R.id.toggleButton6), this);

		flipButtons[2][0].setTb(
				(ToggleButton) findViewById(R.id.toggleButton7), this);
		flipButtons[2][1].setTb(
				(ToggleButton) findViewById(R.id.toggleButton8), this);
		flipButtons[2][2].setTb(
				(ToggleButton) findViewById(R.id.toggleButton9), this);

		// make dialog boxes
		// build winning box
		AlertDialog.Builder winningBoxBuilder = new AlertDialog.Builder(this);
		winningBoxBuilder.setCancelable(false);
		winningBoxBuilder.setPositiveButton(R.string.label_confirm,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						shuffleBoard();
					}
				});
		winningBoxBuilder.setNegativeButton(R.string.label_quit,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
						finish();
					}
				});
		winningBox = winningBoxBuilder.create();

		// build reset box
		AlertDialog.Builder resetBoxBuilder = new AlertDialog.Builder(this);
		resetBoxBuilder.setMessage(R.string.dialog_message_reset);
		resetBoxBuilder.setPositiveButton(R.string.label_confirm,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						shuffleBoard();
					}
				});
		resetBoxBuilder.setNegativeButton(R.string.label_cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		resetBox = resetBoxBuilder.create();

	}

	public void gameWon() {
		//format string so that moves are shown
		String message = String.format(getString(R.string.dialog_message_win), numMoves); 
		winningBox.setMessage(message);
		winningBox.show();
	}

	public void shuffleBoard() {
		// this randomizes the board
		// initilize random number generator
		Random rn = new Random();
		boolean randomBool;
		// this is to make sure they aren't all true for instant victory!
		int boolCounter = 0;

		for (int i = 0; i < HEIGHT; i++) {
			for (int k = 0; k < WIDTH; k++) {
				randomBool = rn.nextBoolean();
				if (randomBool) {
					if (boolCounter < HEIGHT * WIDTH - 1) {
						flipButtons[i][k].tb.setChecked(randomBool);
					} else {
						flipButtons[i][k].tb.setChecked(false);
					}
				} else {
					flipButtons[i][k].tb.setChecked(false);
				}
			}
		}
		numMoves = 0;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_reset) {
			resetBox.show();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		for (int i = 0; i < HEIGHT; i++) {
			for (int k = 0; k < WIDTH; k++) {
				if (v == flipButtons[i][k].getTb()) {
					flipButtons[i][k].flipPanels();
					numMoves++;
				}
			}
		}
		// check to see if all tiles are 'checked'
		for (int i = 0; i < HEIGHT; i++) {
			for (int k = 0; k < WIDTH; k++) {
				if (flipButtons[i][k].getTb().isChecked()) {
					numCheckedTiles++;
				}
			}
		}
		if (numCheckedTiles == HEIGHT * WIDTH) {
			gameWon();
		}
		// reset count
		numCheckedTiles = 0;
	}

}
