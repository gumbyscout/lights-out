package gumbyscout.lightsout;

import android.view.View.OnClickListener;
import android.widget.ToggleButton;

public class FlipButton{
	public ToggleButton tb;
	private FlipButton upButton;
	private FlipButton downButton;
	private FlipButton leftButton;
	private FlipButton rightButton;
	
	//debug bools
	private boolean isEdge;
	private boolean isCorner;
	private boolean isMiddle;

	public FlipButton() {
		//do nothing
	}

	public FlipButton(FlipButton left, FlipButton right, FlipButton up,
			FlipButton down) {
		this.leftButton = left;
		this.rightButton = right;
		this.upButton = up;
		this.downButton = down;
	}

	static public FlipButton[][] generateFlipButtons(int width, int height) {
		// generate linked tiles in an array
		FlipButton[][] fb = new FlipButton[height][width];
		//initialize blank FlipButtons
		for (int i = 0; i < height; i++){
			for (int k = 0; k < width; k++){
				fb[i][k] = new FlipButton();
			}
		}
		// make flip buttons! so difficult!
		// go line by line
		for (int i = 0; i < height; i++) {
			for (int k = 0; k < width; k++) {
				//top left
				if (k == 0 && i == 0) {
					fb[i][k].setAdjacentTiles(null, fb[0][1], null, fb[1][0]);
					//debug
					fb[i][k].isCorner = true;
				}
				// bottom left
				else if (k == 0 && i == height - 1) {
					fb[i][k].setAdjacentTiles(null, fb[height-1][1], fb[height - 2][0], null);
					//debug
					fb[i][k].isCorner = true;
				}
				// top right
				else if (k == width-1 && i == 0) {
					fb[i][k].setAdjacentTiles(fb[0][width - 2], null, null, fb[1][width - 1]);
					//debug
					fb[i][k].isCorner = true;
				}
				// bottom right
				else if (k == width-1 && i == height-1) {
					fb[i][k].setAdjacentTiles(fb[height - 1][width - 2], null, fb[height - 2][width - 1], null);
					//debug
					fb[i][k].isCorner = true;
				}
				
				// find edges
				//left edges
				if (k == 0 && i > 0 && i < height-1){
					fb[i][k].setAdjacentTiles(null, fb[i][k+1], fb[i-1][k], fb[i+1][k]);
					//debug
					fb[i][k].isEdge = true;
				}
				//right edges
				if (k == width-1 && i > 0 && i < height-1){
					fb[i][k].setAdjacentTiles(fb[i][k-1], null, fb[i-1][k], fb[i+1][k]);
					//debug
					fb[i][k].isEdge = true;
				}
				//top edges
				else if (i == 0 && k > 0 && k < width-1) {
					fb[i][k].setAdjacentTiles(fb[0][k-1], fb[0][k+1], null, fb[1][k]);
					//debug
					fb[i][k].isEdge = true;
				}
				//bottom edges
				else if (i == height-1 && k > 0 && k < width-1){
					fb[i][k].setAdjacentTiles(fb[i][k-1], fb[i][k+1], fb[i-1][k], null);
					//debug
					fb[i][k].isEdge = true;
				}
				
				
				//middle bits
				if (i > 0 && i < width-1 && k > 0 && k < height-1){
					fb[i][k].setAdjacentTiles(fb[i-1][k], fb[i+1][k], fb[i][k-1], fb[i][k+1]);
					//debug
					fb[i][k].isMiddle = true;
				}

			}
			
		}
		return fb;
	}
	
	public void flipPanels() {

		// flip adjacent tiles
		if (this.leftButton != null) {
			this.leftButton.tb.toggle();
		}
		if (this.rightButton != null) {
			this.rightButton.tb.toggle();
		}
		if (this.upButton != null) {
			this.upButton.tb.toggle();
		}
		if (this.downButton != null) {
			this.downButton.tb.toggle();
		}
	}
	
	
	public void setAdjacentTiles(FlipButton left, FlipButton right, FlipButton up, FlipButton down){
		this.leftButton = left;
		this.rightButton = right;
		this.upButton = up;
		this.downButton = down;
	}

	@Override
	public String toString() {
		//only works if the buttons actually have text, they did when I used this for debugging
		String string = "";
		if(leftButton != null){
			string += " l:" + leftButton.tb.getText();
		}
		else{
			string += " l:NULL";
		}
		if(rightButton != null){
			string += " r:" + rightButton.tb.getText();
		}
		else{
			string += " r:NULL";
		}
		if(upButton != null){
			string += " u:" + upButton.tb.getText();
		}
		else{
			string += " u:NULL";
		}
		if(downButton != null){
			string += " d:" + downButton.tb.getText();
		}
		else{
			string += " d:NULL";
		}
		return string;
				
	}

	public boolean isEdge() {
		return isEdge;
	}

	public void setEdge(boolean isEdge) {
		this.isEdge = isEdge;
	}

	public boolean isCorner() {
		return isCorner;
	}

	public void setCorner(boolean isCorner) {
		this.isCorner = isCorner;
	}

	public boolean isMiddle() {
		return isMiddle;
	}

	public void setMiddle(boolean isMiddle) {
		this.isMiddle = isMiddle;
	}

	public ToggleButton getTb() {
		return tb;
	}

	public void setTb(ToggleButton tb, Object listener) {
		this.tb = tb;
		this.tb.setOnClickListener((OnClickListener) listener);
	}

	public FlipButton getUpButton() {
		return upButton;
	}

	public void setUpButton(FlipButton upButton) {
		this.upButton = upButton;
	}

	public FlipButton getDownButton() {
		return downButton;
	}

	public void setDownButton(FlipButton downButton) {
		this.downButton = downButton;
	}

	public FlipButton getLeftButton() {
		return leftButton;
	}

	public void setLeftButton(FlipButton leftButton) {
		this.leftButton = leftButton;
	}

	public FlipButton getRightButton() {
		return rightButton;
	}

	public void setRightButton(FlipButton rightButton) {
		this.rightButton = rightButton;
	}
}
